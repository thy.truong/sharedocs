document.addEventListener("DOMContentLoaded", () => {
    let burger = document.querySelector('#burger');
    burger.addEventListener('click', ()=>{
        let menu = document.querySelector('#navbar');
        burger.classList.toggle('open');
        menu.classList.toggle('open');
    })
    let expandTitles = document.querySelectorAll('#side-column [class$="-title"]');
    expandTitles.forEach(title=>{
        title.addEventListener('click', ()=>{
            let underSection = title.parentNode.lastElementChild;
            console.log(underSection);
            underSection.classList.toggle('show');
            title.querySelector('.expand-button').classList.toggle('open');
        })
    })
});
